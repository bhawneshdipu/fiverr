package com.dipu.library;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
	public Library library;

	public Scanner in ;
	public Main() {
		this.library=new Library();
		in=new Scanner(System.in);
	}
	public static void main(String[] args) {
		int input=0;
		Main m=new Main();
		do {
			System.out.println("Library Management System:");
			System.out.println("1.Add Book:");
			System.out.println("2.Add Member:");
			System.out.println("3.Print All Books:");
			System.out.println("4.Print Available Books:");
			System.out.println("5.Print Reserved Books:");
			System.out.println("6.Print Issued Books:");
			System.out.println("7.Issue a Book:");
			System.out.println("8.Submit a Book:");
			System.out.println("9.Pay Fine:");
			System.out.println("10.Search a Book by Linear Search:");
			System.out.println("11.Search a Book by Binary Search:");
			System.out.println("12.List all Transactions:");
			System.out.println("13.Calculate Fine of a Transaction:");
			System.out.println("-1.Exit :");
			
			input=m.in.nextInt();
			m.handleResponse(input);
		}while(input!=-1);
	}
	public  void handleResponse(int input) {
		if(input==1) {
			library.addNewBook();
		}else if(input==2) {
			library.addMember();
		}else if(input==3) {
			library.printAllBooks();
		}else if(input==4) {
			library.printAvailabeBooks();
		}else if(input==5) {
			library.printReservedBooks();
		}else if(input==6) {
			library.printIssuedBooks();;
		}else if(input==7) {
			System.out.println("ENTER ISBN for the BOOK:");
			String isbn=in.next();
			Book b=library.searchForAvailableBookWithISBN(isbn);
			if(b==null) {
				System.out.println("Book With ISBN:"+isbn+" not found:");
			}else {
				System.out.println("ENTER Member card number:");
				String card=in.next();
				Member m=library.searchMemberByCard(card);
				if(m==null) {
					System.out.println("Member not Found with card:"+card);
				}else {
					library.issueBook(b, m);	
				}
				
			}
			
		}else if(input==8) {
			System.out.println("Select Transaction with the book to be submit");
			library.listTransactionsForInput();
			int i=in.nextInt();
			BookTransaction bt=library.findTransactionsByIndex(i);
			library.submitBook(bt);
		}else if(input==9) {
			System.out.println("Select Transaction for which to pay fine");
			library.listTransactionsForInput();
			int i=in.nextInt();
			BookTransaction bt=library.findTransactionsByIndex(i);
			
			library.payFine(bt);
		}else if(input==10) {
			System.out.println("Search Book:\n1. with ISBN\n2. with Title");
			int i=in.nextInt();
			if(i==1) {
				System.out.println("Enter ISBN of the Book");
				String isbn=in.next();
				Book b=library.searchForAllBookWithISBN(isbn);
				if(b==null) {
					System.out.println("Book not Found");
						
				}else {
					System.out.println(b);
				}
			}else if(i==2) {
				System.out.println("Enter Title of the Book");
				String title=in.next();
				Book b=library.searchForAllBookWithTitle(title);
				if(b==null) {
					System.out.println("Book not Found");
						
				}else {
					System.out.println(b);
				}
			}else {
				System.out.println("Invalid Input: Please try again");
					
			}
			
			
		}else if(input==11) {
			System.out.println("Binary Search  Book:\n1. with ISBN\n2. with Title");
			int i=in.nextInt();
			ArrayList<Book>al=library.getAllBooks();
			Book[] items=al.toArray(new Book[al.size()]);
			
			if(i==1) {
				System.out.println("Enter ISBN of the Book");
				String query=in.next();
				
				Book b=Library.searchByName(items, query);
				if(b==null) {
					System.out.println("Book not Found");
						
				}else {
					System.out.println(b);
				}
			}else if(i==2) {
				System.out.println("Enter Title of the Book");
				String query=in.next();
				
				Book b=Library.searchByName(items, query);
				if(b==null) {
					System.out.println("Book not Found");
						
				}else {
					System.out.println(b);
				}
			}else {
				System.out.println("Invalid Input: Please try again");
					
			}
			
		}else if(input==12) {
			library.listTransactions();
		}else if(input==13) {
			System.out.println("Select Transaction for which to pay fine");
			library.listTransactionsForInput();
			int i=in.nextInt();
			BookTransaction bt=library.findTransactionsByIndex(i);
			
			long amount=library.calculateFine(bt);
			System.out.println("Fine for Transaction :"+bt+"\n is :"+amount);
		}else if(input==14) {
			
		}
	}

}
