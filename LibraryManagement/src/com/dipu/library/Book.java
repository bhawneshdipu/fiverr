package com.dipu.library;
import java.util.ArrayList;

public class Book{
	private String isbn;
	private String title;
	private boolean reserved;
	private static int maxCopies=8;
	private int availableCopies;
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public boolean isReserved() {
		return reserved;
	}
	public void setReserved(boolean reserved) {
		this.reserved = reserved;
	}
	
	public int getMaxCopies() {
		return maxCopies;
	}
	
	public int getAvailableCopies() {
		return availableCopies;
	}
	public void setAvailableCopies(int availableCopies) {
		this.availableCopies = availableCopies;
	}
	@Override
	public String toString() {
		return "Book [isbn=" + isbn + ", title=" + title + ", reserved=" + reserved + ", maxCopies=" + maxCopies
				+ ", availableCopies=" + availableCopies + "]";
	}

	
	
	
		
}