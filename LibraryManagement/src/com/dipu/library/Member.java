package com.dipu.library;

public class Member {

	private String memberName;
	private String memberCard;
	private int memberIssuedBookCount;
	private int duefine;
	private boolean isBlacklisted;
	public int getDuefine() {
		return duefine;
	}
	
	public void setDuefine(int duefine) {
		this.duefine = duefine;
	}
	public boolean isBlacklisted() {
		return isBlacklisted;
	}
	public void setBlacklisted(boolean isBlacklisted) {
		this.isBlacklisted = isBlacklisted;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getMemberCard() {
		return memberCard;
	}
	public void setMemberCard(String memberCard) {
		this.memberCard = memberCard;
	}
	public int getMemberIssuedBookCount() {
		return memberIssuedBookCount;
	}
	public void setMemberIssuedBookCount(int memberIssuedBookCount) {
		this.memberIssuedBookCount = memberIssuedBookCount;
	}
	@Override
	public String toString() {
		return "Member [memberName=" + memberName + ", memberCard=" + memberCard + ", memberIssuedBookCount="
				+ memberIssuedBookCount + ", duefine=" + duefine + ", isBlacklisted=" + isBlacklisted + "]";
	}
	
}
