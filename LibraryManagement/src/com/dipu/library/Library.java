package com.dipu.library;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Scanner;
public class Library {
	ArrayList<Book> allBooks;
	ArrayList<Book> issuedBooks;
	ArrayList<Book> reservedBooks;
	ArrayList<Book> availableBooks;
	ArrayList<Member> members;
	ArrayList<BookTransaction> transactions;
	
	public ArrayList<Book> getAllBooks() {
		return allBooks;
	}
	public void setAllBooks(ArrayList<Book> allBooks) {
		this.allBooks = allBooks;
	}
	public ArrayList<Book> getIssuedBooks() {
		return issuedBooks;
	}
	public void setIssuedBooks(ArrayList<Book> issuedBooks) {
		this.issuedBooks = issuedBooks;
	}
	public ArrayList<Book> getReservedBooks() {
		return reservedBooks;
	}
	public void setReservedBooks(ArrayList<Book> reservedBooks) {
		this.reservedBooks = reservedBooks;
	}
	public ArrayList<Book> getAvailableBooks() {
		return availableBooks;
	}
	public void setAvailableBooks(ArrayList<Book> availableBooks) {
		this.availableBooks = availableBooks;
	}
	
	// Library Functions
	
	
	//Book Print Functions
	public void printAllBooks() {
		for(Book b :allBooks) {
			System.out.println(b);
		}
	}
	public void printIssuedBooks() {
		for(Book b :issuedBooks) {
			System.out.println(b);
		}
	}
	public void printAvailabeBooks() {
		for(Book b :availableBooks) {
			System.out.println(b);
		}
	}
	public void printReservedBooks() {
		for(Book b :reservedBooks) {
			System.out.println(b);
		}
	}
	//Book Count functions
	public int countAllBooks() {
		return allBooks.size();
	}
	public int countIssuedBooks() {
		return issuedBooks.size();
	}
	public int countAvailabeBooks() {
		return availableBooks.size();
	}
	public int countReservedBooks() {
		return reservedBooks.size();
	}
	public int countOfEachBook() {
		int count=0;
		for(Book b :allBooks) {
			count+=b.getMaxCopies();
		}
		return count;
	}
	public int countOfEachAvailableBook() {
		int count=0;
		for(Book b :allBooks) {
			count+=b.getAvailableCopies();
		}
		return count;
	}
	
	
	//Library Operations
	public boolean addNewBook() {
		//add to all books;
		Book b=new Book();
		System.out.println("Add new Book :\n");
		Scanner in =new Scanner(System.in);
		System.out.println("Enter ISBN:\n");
		
		String isbn=in.nextLine();
		b.setIsbn(isbn);
		
		System.out.println("Enter Title:\n");
		String title=in.nextLine();
		b.setIsbn(title);
		
		this.allBooks.add(b);
		return true;
		
	}
	
	public boolean issueBook(Book b,Member m) {
		//check if book is not reserved and count is greater than 0;
		if(b.isReserved()==false && b.getAvailableCopies()>=1) {
			//check if member is blacklisted
			if(m.isBlacklisted()) {
				System.out.println("Sorry,Member is blacklisted:"+m);
				return false;
			}else {
				BookTransaction t=new BookTransaction();
				t.setBook(b);
				t.setMember(m);
				LocalDateTime ldt=LocalDateTime.now();
				t.setDate(ldt);
				//dectease book count by 1;
				b.setAvailableCopies(b.getAvailableCopies()-1);
				//check if book count is 0 then set it to reseve:
				if(b.getAvailableCopies()==0) {
					b.setReserved(true);
				}
				
				m.setMemberIssuedBookCount(m.getMemberIssuedBookCount()+1);
				//add Transaction to transaction history
				transactions.add(t);
				return true;
			}
		}else {
			System.out.println("Sorry,Book not available for now:"+b);
		}
		return false;
		
	}
	public boolean submitBook(BookTransaction t) {
		Book b=t.getBook();
		Member m=t.getMember();
		long fine=calculateFine(t);
		if(fine==0) {
			t.setActive(false);
			m.setMemberIssuedBookCount(m.getMemberIssuedBookCount()-1);
			b.setAvailableCopies(b.getAvailableCopies()+1);
			if(b.isReserved()) {
				reservedBooks.remove(b);
				availableBooks.add(b);
			}
			return true;
		}
		else {
			System.out.println("You Have Due Fine , Please Pay Fines");
			return false;
		}
	}
	public boolean payFine(BookTransaction t) {
		//check if transaction is active or closed
		if(t.isActive()) {
			Member m=t.getMember();
			Book b=t.getBook();
			long fine=calculateFine(t);
			Scanner in=new Scanner(System.in);
			System.out.println("Member  Total Fine is :"+fine+"\nEnter amount not greater than fine :\n");
			int amount=in.nextInt();
			if(amount>=fine) {
				m.setDuefine(0);
				t.setActive(false);
			}
			return true;
		}else {
			System.out.println("Transaction :"+t+" is closed.");
			return false;
		}
		
		
		
		
		
	}
	
	// Member Operaions
	public Member addMember() {
		System.out.println("Add Member to Library:\n");
		Member m=new Member();
		Scanner in =new Scanner(System.in);
		
		System.out.println("Enter Member Name:");
		String name=in.nextLine();
		m.setMemberName(name);
		System.out.println("Enter Member Card:");
		String card=in.next();
		m.setMemberCard(card);
		return m;
		
	}
	public boolean removeMember(Member m) {
		for(Member obj:members) {
			if(obj.equals(m)) {
				members.remove(obj);
				return true;
			}
		}
		return false;
	}
	
	
	
	
	//Largest count
	public int findLargestCountofBookAvailable() {
		Book temp=availableBooks.get(0);
		for(Book b : availableBooks) {
			if(b.getAvailableCopies()>temp.getAvailableCopies()) {
				temp=b;
			}
		}
		return temp.getAvailableCopies();
	}
	//Search for reserved book 
	public Book searchForReservedBookWithISBN(String ISBN) {
		for(Book b:reservedBooks) {
			if(b.getIsbn().equalsIgnoreCase(ISBN)){
				return b;
			}
		}
		return null;
	}
	public Book searchForReservedBookWithTitle(String title) {
		for(Book b:reservedBooks) {
			if(b.getTitle().equalsIgnoreCase(title)){
				return b;
			}
		}
		return null;
	}
	
	//Search for available book 
		public Book searchForAvailableBookWithISBN(String ISBN) {
			for(Book b:availableBooks) {
				if(b.getIsbn().equalsIgnoreCase(ISBN)){
					return b;
				}
			}
			return null;
		}
		public Book searchForAvailableBookWithTitle(String title) {
			for(Book b:availableBooks) {
				if(b.getTitle().equalsIgnoreCase(title)){
					return b;
				}
			}
			return null;
		}
		//Search for all book 
		
		public Book searchForAllBookWithISBN(String ISBN) {
			for(Book b:allBooks) {
				if(b.getIsbn().equalsIgnoreCase(ISBN)){
					return b;
				}
			}
			return null;
		}
		public Book searchForAllBookWithTitle(String title) {
			for(Book b:allBooks) {
				if(b.getTitle().equalsIgnoreCase(title)){
					return b;
				}
			}
			return null;
		}
		
	//search member by cardnumber
		public Member searchMemberByCard(String card) {
			for(Member m : members) {
				if(m.getMemberCard().equalsIgnoreCase(card)) {
					return m;
				}
			}
			return null;
		}
		
	//search by name ==> Binary Search applies on Sorted array in accending order;

	public static Book searchByName(Book[] items, String name) {
		//assumning array is sorted
		int left=0;
		int right=items.length-1;
		
		while(left<right) {
			int middle=(left+right)/2;
			if(items[middle].getIsbn().equals(name)) {
				return items[middle];
			}else {
				if(items[middle].getIsbn().compareTo(name)>0) {
					right=middle-1;
				}else {
					left=middle+1;
				}
			}
		}
		return null;
	}


	
	public void listTransactionsForInput() {
		int i=0;
		for(BookTransaction bt:transactions) {
			System.out.println(i+" ==> "+ bt);
			i++;
		}
	}
	public void listTransactions() {
		for(BookTransaction bt:transactions) {
			System.out.println(bt);
		}
	}
	public BookTransaction findTransactionsByIndex(int i) {
		if(i<transactions.size()) {
			return transactions.get(i);
		}
		return null;
	}
	
	//calculate fine for the tranaction on member
	public long calculateFine(BookTransaction t) {
		LocalDateTime ltd=t.getDate();
		LocalDateTime now=LocalDateTime.now();
		long countdays=ChronoUnit.DAYS.between(now,ltd);
		//assumning the count of days as a unit of fine
		if(countdays>=14) {
			return countdays-14;
		}else {
			return 0;
		}
	}

	
	
}
