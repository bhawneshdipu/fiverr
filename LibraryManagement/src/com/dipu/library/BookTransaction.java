package com.dipu.library;

import java.time.LocalDateTime;

public class BookTransaction {
	
	private Book book;
	private Member member;
	private LocalDateTime date;
	private boolean isActive;
	public Book getBook() {
		return book;
	}
	public void setBook(Book book) {
		this.book = book;
	}
	
	public Member getMember() {
		return member;
	}
	public void setMember(Member member) {
		this.member = member;
	}
	public LocalDateTime getDate() {
		return date;
	}
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	public void setDate(LocalDateTime date) {
		this.date = date;
	}
	@Override
	public String toString() {
		return "BookTransaction [book=" + book + ", member=" + member + ", date=" + date + ", isActive=" + isActive
				+ "]";
	}
	
}
