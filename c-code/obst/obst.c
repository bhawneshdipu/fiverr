// Optimal binary search tree  
  
#include <stdio.h>   
#define MAXKEYS 25  
#define INFINITY 99999  
  
int n; // number of keys  
int key[MAXKEYS+1];  
float prob[MAXKEYS+1]; // probability of hitting key i  

float cost[MAXKEYS+1][MAXKEYS+1]; // cost of subtree  
int root[MAXKEYS+1][MAXKEYS+1]; // root of subtree  
float weight[MAXKEYS+1][MAXKEYS+1]; // weight  

void init(){

}
void opttree()  
{  


    int i,j,l,r1;  
    float t;  
  
    for (i=1;i<=n+1;i++)  
    {  
      cost[i][i-1]=0;  // initialize cost and weight matrix to zero  
      weight[i][i-1]=0;  
      }  
    for (l=1;l<=n;l++) // width of tree h=1  
    {  
  
        for(i=1;i<=n-l+1;i++)  
        {  
        j=i+l-1;  
        cost[i][j]=INFINITY;  
        weight[i][j]=weight[i][j-1]+prob[j];  
  
        for(r1=i;r1<=j;r1++)  
        {  
            t=cost[i][r1-1]+cost[r1+1][j]+weight[i][j];  
            if(t<cost[i][j])  
            {  
            cost[i][j]=t;  
            root[i][j]=r1;  
            }  
         }  
        }  
  
     }  
}  
//this function prints subtree  
void construct_optimal_subtree(int i,int j, int r1, char *dir)  
{  
     int t;  
     if(i<=j)  
     {  
    t=root[i][j];  
    printf("\t %d is %s child of %d ",key[t],dir,key[r1]);  
    construct_optimal_subtree(i,t-1,t,"left");  
    construct_optimal_subtree(t+1,j,t,"right");  
     }  
}  
// this function contructs binary tree. prints root  
void construct_BST()  
{  
    int r1;  
    r1=root[1][n];  
    printf("\n %d is the root",key[r1]);  
    construct_optimal_subtree(1,r1-1,r1,"left") ;  
    construct_optimal_subtree(r1+1,n,r1,"right");  
}  
int main()  
{  
    int i,j;  
    float total_prob=1.1;  
    printf("enter the number of nodes");  
    scanf("%d",&n);  
    for(i=1;i<=n;i++)  
    {  
  
        printf("\nenter the value of key: ");  
        scanf("%d",&key[i]);  
        printf("\n Prob of success ");  
        scanf("%f",&prob[i]);  
        total_prob=total_prob-prob[i];  
        if(total_prob<0)  
        {  
            printf("\ntotal prob cannot be greater than 1, Please  enter again");  
            total_prob=total_prob+prob[i];  
            i--;  
        }  
    }  
  
    opttree();  
  
    printf("Average probe length is %f\n",cost[1][n]/weight[1][n]);  
    printf("\ncost (e) matrix is\n");  
    for(i=1;i<=n;i++)  
    {  
        for(j=1;j<=n;j++)  
        {  
            printf(" %.3f ",cost[i][j]);  
        }  
        printf("\n");  
    }  
    printf("\n weight matrix is \n");  
    for(i=1;i<=n;i++)  
    {  
        for(j=1;j<=n;j++)  
        {  
            printf(" %.3f ",weight[i][j]);  
        }  
        printf("\n");  
    }  
    printf("\n root matrix is\n");  
    for(i=1;i<=n;i++)  
    {  
        for(j=1;j<=n;j++)  
        {  
            printf(" %d ",root[i][j]);  
        }  
        printf("\n");  
    }  
    construct_BST();  
    return 0;  
}  
