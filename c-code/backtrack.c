#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define MAX 10
typedef struct Knapsack{
    int profit;
    int weight;
    int subsetw[MAX];
    int subsetv[MAX];
    
    int k;
} KnapsackData;

KnapsackData backtrack(int W,int wt[],int val[],int N,int k,KnapsackData kd){
    int profit=0;
    int weight=0;
    int i=0;
    KnapsackData tkd;
    tkd.profit=0;
    tkd.weight=0;
    tkd.k=0;
    if(W==0 || N==0){
        return tkd;
    }
    if(wt[N-1]>W){
        tkd=backtrack(W,wt,val,N-1,k,kd);
        //printf("skip item:%d\n",tkd.profit);
    }else{
        tkd=kd;
        //printf("set item at :%d ==> %d\n",k,wt[N-1]);
        
        kd=backtrack(W-wt[N-1],wt,val,N-1,k+1,kd);
        kd.profit=val[N-1]+kd.profit;
        kd.weight=wt[N-1]+kd.weight;
        kd.k=kd.k+1;
        kd.subsetw[k]=wt[N-1];
        kd.subsetv[k]=val[N-1];
        
        
        //printf("else add item:%d\n",kd.profit);
        
        tkd=backtrack(W,wt,val,N-1,k,tkd);

        //printf("else skip item:%d\n",tkd.profit);
        
        if(kd.profit<tkd.profit){
            return tkd;
        }else{
            return kd;
        }  
    }    
    
}

int main(int argc, char *args[]){
    FILE *fp;
    //printf("%s %s ",args[0],args[1]);
    fp = freopen(args[1], "r+", stdin);
    int N=0;
    scanf("%d\n",&N);
    //printf("Number of Elements:%d\n",N);
    int W=0;
    scanf("%d\n",&W);
    //printf("Maximum Weight:%d\n",W);
    int val[MAX];
    for(int i=0;i<N;i++){
        int tmp=0;
        scanf("%d,",&tmp);
        val[i]=tmp;
        //printf("Scanned Val:%d,",tmp);
    }
    int wt[MAX];
    for(int i=0;i<N;i++){
        int tmp=0;
        scanf("%d,",&tmp);
        wt[i]=tmp;
        //printf("Scanned Wt:%d,",tmp);
   
    }
    // There are 2^n possible subset of the items;
    int subsetCount=pow(2,N);
    KnapsackData kd;
    kd.profit=0;
    kd.weight=0;
    
    for(int i=0;i<subsetCount;i++){
        KnapsackData tmpKd=backtrack(W,wt,val,N,0,kd);
        if(tmpKd.profit>kd.profit && tmpKd.weight<=W){
            kd=tmpKd;
        }
    }
    printf("Final Result of Knapsack:\nprofit: %d \nweight: %d \n",kd.profit,kd.weight);
    printf("Items:[");
    for(int i=0;i<kd.k;i++){
           printf(" %d ==> %d ,",kd.subsetw[i],kd.subsetv[i]);
        
    } 
    printf("]\n");

}