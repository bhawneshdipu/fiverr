#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX 20
typedef struct node{
    struct node *left,*right;
    int frequency;
    int character;
} Node;
typedef struct priority_queue{
    Node arr[MAX];
    int size;
} priority_queue;
/* global Variables*/

priority_queue queue;

Node create_node(char ch,int freq){
        Node new_node;
        new_node.frequency=freq;
        new_node.character=ch;
        new_node.left=NULL;
        new_node.right=NULL;

        return new_node;
}
Node insert_node(Node new_node){
    int i=queue.size;
    int j= i-1;
    while (j >= 0 && queue.arr[j].frequency > new_node.frequency)
       {
           queue.arr[j+1] = queue.arr[j];
           j = j-1;
       }
       queue.arr[j+1] = new_node;
       queue.size=queue.size+1;
}

Node extract_min(){
    Node first=queue.arr[0];
    
    for(int i=0;i<queue.size;i++){
        queue.arr[i]=queue.arr[i+1];
    }
    queue.size=queue.size-1;
    return first;
}
void init_queue(char chr[],int f[],int n){
    queue.size=0;
    queue.arr[MAX];

    for(int i=0;i<n;i++){
        Node X=create_node(chr[i],f[i]);
        insert_node(X);
    }
     printf("Init Done\n"); 
    for(int i=0;i<n;i++){
        printf("%c ==> %d\n",queue.arr[i].character,queue.arr[i].frequency);
    }
}
void build_hoffman(){
    while(queue.size>1){
        Node left=extract_min();
        Node right=extract_min();
        Node X;
        X.frequency=left.frequency+right.frequency;
        X.character='#';
        X.left=&left;
        X.right=&right;
        insert_node(X);
        printf("Building :\n"); 
        for(int i=0;i<queue.size;i++){
            printf("%c ==> %d\n",queue.arr[i].character,queue.arr[i].frequency);
        }
    }
    printf("HOFFMAN Done\n"); 
    

}
int main(int argv,char** args ){
    FILE *fp;
    printf("%s %s \n",args[0],args[1]);
    fp = freopen(args[1], "r+", stdin);
    char line[1024];
    scanf("%[^\n] ",line);
    printf("line:%s\n",line);
    char chr[MAX];
    int size=0;
    int i=0;
    int k=0;
    char x;
    
    while(line[i]!='\0'){
        x=line[i];
        //printf("%c \n",x);
        if(x==' ' || x==','){
            //printf("Skip:%c\n",x);
            i++;
            continue;
        }else{
            chr[k++]=x;
            //printf("Taken:%c\n",chr[k-1]);
            i++;
            size++;
        }
    }
    
    scanf("%[^\n]",line);
    printf("line:%s\n",line);
    char* token=strtok(line," ,");
    int freqArr[MAX];
     i=0;
    while(token) {
        //printf("%s\n",token);
        freqArr[i]=atoi(token);
        //printf("Freq:%d\n",freqArr[i]);
        i++;
        token = strtok(NULL, " ,");
    }
    printf("Size:%d\n",size);
    
    for(int i=0;i<size;i++){
        printf("%c ==> %d\n",chr[i],freqArr[i]);
    }
    init_queue(chr,freqArr,size);
    build_hoffman();
}