#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define MAX 50
int max(int a,int b){
    if(a>b){
        return a;
    }else{
        return b;
    }
}
typedef struct Knapsack{
    int profit;
    int weight;
    int dp[MAX][MAX];
    int items[MAX];
    int k;
} KnapsackData;

KnapsackData dynamic(int W,int wt[],int val[],int N){
    KnapsackData tkd;
    tkd.profit=0;
    tkd.weight=0;
    for(int i=0;i<=N;i++){
        tkd.items[i]=0;
        for(int j=0;j<=W;j++){
            tkd.dp[i][j]=0;
        }
    }

    for (int i=1;i<=N;i++){
        for(int j=0;j<=W;j++){
            if(wt[i]>j){
                tkd.dp[i][j]=tkd.dp[i-1][j];
            }else{
                tkd.dp[i][j]=max(tkd.dp[i-1][j],tkd.dp[i-1][j-wt[i]]+val[i]);
            }
        }
    }

    tkd.profit=tkd.dp[N][W];

    int j=W;
    int i=N;
    int weight=0;
    int k=0;
    tkd.k=0;
    while (i > 0 && j > 0 ){
        if ((tkd.dp[i][j] - tkd.dp[i-1][j-wt[i-1]]) == val[i-1]){
            //printf("Item Taken:\n");
            weight += wt[i-1];
            tkd.items[k]=wt[i-1];
            k++;
            tkd.k=k;
            j = j - wt[i-1];
            i = i - 1;
        }
        else{
            i = i - 1;
        }

    }
    tkd.weight=weight;
    return tkd;
}


int main(int argc, char *args[]){
    FILE *fp;
    //printf("%s %s ",args[0],args[1]);
    fp = freopen(args[1], "r+", stdin);
    int N=0;
    scanf("%d\n",&N);
    printf("Number of Elements:%d\n",N);
    int W=0;
    scanf("%d\n",&W);
    printf("Maximum Weight:%d\n",W);
    int val[MAX];
    for(int i=0;i<N;i++){
        int tmp=0;
        scanf("%d,",&tmp);
        val[i]=tmp;
        //printf("Scanned Val:%d,",tmp);
    }
    int wt[MAX];
    for(int i=0;i<N;i++){
        int tmp=0;
        scanf("%d,",&tmp);
        wt[i]=tmp;
        //printf("Scanned Wt:%d,",tmp);
   
    }
    // There are 2^n possible subset of the items;
    KnapsackData kd;
    kd=dynamic(W,wt,val,N);
        
    printf("Final Result of Knapsack:\nprofit: %d \nweight: %d \n",kd.profit,kd.weight);
    printf("Items:[");
    for(int i=0;i<kd.k;i++){
            printf("%d ,",kd.items[i]);
        
    } 
    printf("]\n");

}

