#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define MAX 10
typedef struct Knapsack{
    int profit;
    int weight;
    int subset;
} KnapsackData;

KnapsackData getprofit(int subset,int wt[],int val[],int n){
    int profit=0;
    int weight=0;
    int i=0;
    KnapsackData tkd;
    tkd.subset=subset;
    //printf("Subset:%d\n",subset); 
    for(int i=0;i<n;i++){
        //printf("Subset :%d : %d : %d\n",i,subset,(1<<i));
        //printf("Subset: Ith Ele :%d\n",(subset)&(1<<i));
        if((subset)&(1<<i)){
            profit+=val[i];
            weight+=wt[i];
        }
    }
    //printf("Profit : %d\n,Weight: %d\n",profit,weight);
    tkd.profit=profit;
    tkd.weight=weight;

    return tkd;
}

int main(int argc, char *args[]){
    FILE *fp;
    //printf("%s %s ",args[0],args[1]);
    fp = freopen(args[1], "r+", stdin);
    int N=0;
    scanf("%d\n",&N);
    //printf("Number of Elements:%d\n",N);
    int W=0;
    scanf("%d\n",&W);
    //printf("Maximum Weight:%d\n",W);
    int val[MAX];
    for(int i=0;i<N;i++){
        int tmp=0;
        scanf("%d,",&tmp);
        val[i]=tmp;
        //printf("Scanned Val:%d,",tmp);
    }
    int wt[MAX];
    for(int i=0;i<N;i++){
        int tmp=0;
        scanf("%d,",&tmp);
        wt[i]=tmp;
        //printf("Scanned Wt:%d,",tmp);
   
    }
    // There are 2^n possible subset of the items;
    int subsetCount=pow(2,N);
    KnapsackData kd;
    kd.subset=0;
    kd.profit=0;
    kd.weight=0;
    
    for(int i=0;i<subsetCount;i++){
        KnapsackData tmpKd=getprofit(i,wt,val,N);
        if(tmpKd.profit>kd.profit && tmpKd.weight<=W){
            kd=tmpKd;
        }
    }
    printf("Final Result of Knapsack:\nprofit: %d \nweight: %d \n",kd.profit,kd.weight);
    printf("Items:[");
    for(int i=0;i<N;i++){
        if((kd.subset)&(1<<i)){
            printf("%d ==> %d ,",wt[i],val[i]);
        }
    } 
    printf("]\n");

}

